void splash(){
  
  delay(100);
  GLCD.SelectFont(ktm20);
  GLCD.GotoXY(0,0);
  GLCD.print("READY"); 
  delay(300);
  GLCD.GotoXY(0,20);
  GLCD.print("TO");
  delay(300);
  GLCD.GotoXY(0,40);
  GLCD.print("RACE");
  delay(200);
  GLCD.DrawBitmap(freccetta, 28, 24);
  delay(200);
  GLCD.DrawBitmap(freccetta, 35, 24);
  delay(300);
  GLCD.SelectFont(SystemFont5x7);
  GLCD.GotoXY(70,57);
  GLCD.Puts("rev. 0.2");
  delay(1000);
  
  for(int pixel=0;pixel<64;pixel++){  
  GLCD.SetPixels(0,0,127,pixel,BLACK);
  }
  for(int pixel=0;pixel<64;pixel++){  
  GLCD.SetPixels(63,0,63+pixel,63,WHITE);
  GLCD.SetPixels(62-pixel,0,62,63,WHITE);
  }
  GLCD.ClearScreen();
  
}
