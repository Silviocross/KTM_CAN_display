void readCAN(){
  
      CAN.readMsgBuf(&len, buf);    // read data,  len: data length, buf: data buf
    canId = CAN.getCanId();       // get CAN ID


    if (canId == 0x200) {
      temp = buf[4];
      soc = (int)temp;
      temp = buf[3];
      pof = (int)temp;
    }
    else if (canId == 0x205 && menu==1) {
      temp = buf[1];
      voltage = ((float)temp) * 25.6;
      temp = buf[0];
      voltage = voltage + (float)temp / 10;
      temp = buf[3];
      temperaturemin = (int)temp;
      temp = buf[4];
      temperaturemax = (int)temp;
      temp = buf[5];
      voltagemin = ((float)temp) / 50;
      temp = buf[6];
      voltagemax = ((float)temp) / 50;
    }
    else if (canId == 0x302 && menu==1) {
      temp = buf[2];
      temperatureA = (int)temp-15; 
      temp = buf[3];
      temperatureB = (int)temp-15; 
      temp = buf[4];
      temperatureECU = (int)temp-15;      
      }
      else if (canId == 0x303 && menu==1) {
      temp = buf[0];
      temperatureLV = (int)temp-15; 
      }
  }


