#include <Canbus.h>
#include <defaults.h>
#include <global.h>
#include <mcp2515.h>
#include <mcp2515_defs.h>


#include <SPI.h>
#include "mcp_can.h"


// include the library header
// no font headers have to be included
#include <openGLCD.h>
#include <bitmaps/freccetta.h>
#include <bitmaps/KTMlogo.h>


#define SPI_CS_PIN 10
#define LCDUPDATECYCLE 2000
#define LONGPRESSDELAY 3000
#define BUTTON1 2 // menu button
#define BUTTON2 1 // laptimer button

boolean button2status = false;

unsigned int canId = 0x00;

unsigned char len = 0;
unsigned char buf[8];
unsigned char temp;

unsigned laptime[11];
int currentLap = 0;


long lastUpdate = -25000;
long lastButtonPress = 0;
long firstPress = 0;

int soc = 0;  // initializing values
int previousSoc = soc;
int soh = 0;
int voltage = 0;
float voltagemin = 0;
float voltagemax = 0;
int temperaturemin = 0;
int temperaturemax = 0;
int pof = 0;
int temperatureA = 0;
int temperatureB = 0;
int temperatureECU = 0;
int temperatureLV = 0;

int menu = 0;

MCP_CAN CAN(SPI_CS_PIN);                                    // Set CS pin

void setup()
{
  pinMode(BUTTON1, INPUT);
  
  // Initialize the GLCD
  GLCD.Init();

  Serial.begin(115200);
  Serial.print("Serial Setup");
/*
START_INIT:
  if (CAN_OK == CAN.begin(CAN_500KBPS))                  // init can bus : baudrate = 500k
  {
    Serial.println("CAN BUS init ok!");
  }
  else
  {
    Serial.println("CAN BUS init fail");
    Serial.println("Init CAN BUS again...");
    delay(100);
    goto START_INIT;
  }
*/
Serial.print("Showing splash");
  // splash screen
  splash();

    CAN.init_Mask(0, 0, 0xFFC0000);                         // there are 2 mask in mcp2515, you need to set both of them
    CAN.init_Mask(1, 0, 0xFFC0000);
    
    CAN.init_Filt(0, 0, 0x8000000);   //0x200
    CAN.init_Filt(1, 0, 0x8140000);   //0x205
}

void loop()
{

//  readButtons();
//  readCAN();
  GLCD.print("SOC");

if(millis()-lastUpdate > LCDUPDATECYCLE){  // Update screen every 2s

  switch (menu) {
    case 0:
      if (soc < previousSoc) {
        GLCD.FillRect(0, 0, 128, 57, WHITE);
        previousSoc = soc;
      }
      GLCD.SelectFont(SystemFont5x7);
      GLCD.GotoXY(0, 0);
      GLCD.print("SOC");

      GLCD.SelectFont(CalBlk60);
      GLCD.GotoXY(36, 0);
      GLCD.print(soc);

      // progress bar
      GLCD.DrawRect(0, 57, 128, 7);
      GLCD.FillRect(1, 58, soc * 1.26, 5, BLACK);
      GLCD.FillRect((soc * 1.26 + 1), 58, (126 - soc * 1.26), 5, WHITE);
      break;
   
   case 1:
      GLCD.SelectFont(SystemFont5x7);
      GLCD.GotoXY(0, 0);
      GLCD.print("SOC: ");
      GLCD.print(soc);
      if(soc<10)
        GLCD.FillRect(36,0,10,7,WHITE);

      GLCD.GotoXY(72, 0);
      GLCD.print("T_A: ");
      GLCD.print(temperatureA);
      GLCD.print("C");
            
      GLCD.DrawHLine(0,8,128,BLACK);
      GLCD.GotoXY(0, 11);
      GLCD.print("SOH: ");
      GLCD.print(soh);

      GLCD.GotoXY(72, 11);
      GLCD.print("T_B: ");
      GLCD.print(temperatureB);
      GLCD.print("C");
      
      GLCD.DrawHLine(0,20,128,BLACK);
      GLCD.GotoXY(0, 23);
      GLCD.print("Vmin: ");
      GLCD.print(voltagemin);

      GLCD.GotoXY(72, 23);
      GLCD.print("Tecu: ");
      GLCD.print(temperatureECU);
      GLCD.print("C");
      
      GLCD.DrawHLine(0,31,128,BLACK);
      GLCD.GotoXY(0, 34);
      GLCD.print("Vmax: ");
      GLCD.print(voltagemax);

      GLCD.GotoXY(72, 34);
      GLCD.print("T_LV: ");
      GLCD.print(temperatureLV);
      GLCD.print("C");
      
      GLCD.DrawHLine(0,42,128,BLACK);
      GLCD.GotoXY(0, 45);
      GLCD.print("Tmin: ");
      GLCD.print(temperaturemin);
      GLCD.DrawHLine(0,54,128,BLACK);
      GLCD.GotoXY(0, 56);
      GLCD.print("Tmax: ");
      GLCD.print(temperaturemax);
   break;
  }
  lastUpdate = millis();
}
}
